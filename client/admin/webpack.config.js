const path = require('path');

export default (webpackConfig) => {
  webpackConfig.module.rules.push({
    test: /\.less$/,
    use: ['less-loader', {
        loader: 'style-resources-loader',
        options: {
            patterns: [
                path.resolve(__dirname, './src/assets/less/index.less')
            ]
        }
    }],
    exclude: /node_modules/
  });
  return webpackConfig;
}