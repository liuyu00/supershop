import {get, post, remove} from '@/utils/request';

export const addClassify = (options) => post('/api/product/category', options);
export const getClassify = () => get('/api/product/category');
export const deleteClassify = (options) => remove('/api/product/category', options)


export const getWare = (options) => get('/api/product/duodian/ware', options)
export const getWareDetail = (options) => get('/api/product/duodian/wareDetail', options)
export const getCategory = () => get('/api/product/duodian/category')

export const addProduct = (options) => post('/api/product/add_ware', options)
export const getProduct = (options) => get('/api/product/select_ware', options)


