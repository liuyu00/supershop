import {get, post} from '@/utils/request';


export const getBanner = () => get('/api/home/banner');

export const postBanner = (options) => post('/api/home/banner', options);