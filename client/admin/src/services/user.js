import {post} from '@/utils/request';

export const login = (options) => post('/api/login', options);