import fetch from 'dva/fetch';
import {message} from 'antd';


/**
 * 对象转换为query查询参数
 * @param {Object} obj  {name: 1111, name2: 333}
 * @return {String} 'name=1111&name2=333'
 */
const queryString = (obj) => {
  let str = ''
  Object.keys(obj).forEach((key) => {
    const value = typeof obj[key] === 'object' ? JSON.stringify(obj[key]) : obj[key]
    console.log(key)
    str += `${key}=${value}&`
  })
  return str.substr(0, str.length - 1)
}

const baseOptions = {
  headers: {
    'content-type': 'application/json; charset=utf-8'
  },
  mode: 'cors',
  credentials: 'include'
}

/**
 * 封装fetch
 * @param {String} url  接口地址
 * @param {String} method  请求方法
 * @param {Object} data  传递数据
 */
const request = (url, method, data) => {

  let options = {}
  
  if (method === 'GET') {
    options = {
      ...baseOptions,
      method: method
    }
    url += `?${queryString(data)}`
  } else {
    options = {
      ...baseOptions,
      method: method,
      body: JSON.stringify(data)
    }
  }

  return fetch(url, options).then((response) => {
    if (response.ok) {
      return response.json()
    } else {
      if (response.status === 401) {
        alert('登录')
      } else if (response.status === 422) {
        response.json().then((res) => {
          message.error(res.message);
        })
      }
      return Promise.reject()
    }
  })
}

export const get = (url, data = {}) => {
  return request(url, 'GET', data)
}
export const post = (url, data = {}) => {
  return request(url, 'POST', data)
}
export const remove = (url, data = {}) => {
  return request(url, 'DELETE', data)
}