import React, {Component} from 'react';
import {Button, Card, Icon, Row, Col, Modal, Form, Input, DatePicker} from 'antd';
import {connect} from 'dva';

// import routeConfig from '@/components/RouteConfig'
import PageContent from '@/components/PageContent'

import styles from './style.less'



/**
 * Banner卡片组件
 * @param {Obejct} Props 卡片展示需要的数据
 */
class BannerCard extends Component {

  handlerRemoveClick = () => {
    console.log('删除')
  }

  handlerEditClick = () => {
    console.log('编辑')
  }

  handlerCopyClick = () => {
    console.log('拷贝链接')
  }

  render () {
    const {image} = this.props

    return (
      <div className={styles.banner_card}>
      <Card
        cover={
          <img alt="example" src={image} />
        }
        actions={[
          <a href="javascript:;" onClick={this.handlerRemoveClick}><Icon type="delete" /></a>, 
          <a href="javascript:;" onClick={this.handlerEditClick}><Icon type="edit" /></a>, 
          <a href="javascript:;" onClick={this.handlerCopyClick}><Icon type="copy" /></a>
        ]}
      >
      </Card>
      </div>
    )
  }
}

const BannerList = ({list}) => {
  return (
    <Row gutter={16}>
      {
        list.map(item => (
          <Col span={6} key={item.id}>
            <BannerCard {...item} />
          </Col>
        ))
      }
    </Row>
  )
}

@Form.create()
class AddForm extends Component {

  render () {
    const {getFieldDecorator} = this.props.form;
    return (
      <Form>
        <Form.Item label="活动名称">
          {getFieldDecorator('active_name', {})(<Input />)}
        </Form.Item>
        <Form.Item label="活动地址">
          {getFieldDecorator('url', {})(<Input />)}
        </Form.Item>
        <Form.Item label="图片地址">
          {getFieldDecorator('image', {})(<Input />)}
        </Form.Item>
        <Form.Item label="活动日期">
          {getFieldDecorator('time', {})(<DatePicker.RangePicker />)}
        </Form.Item>
      </Form>
    )
  }
}

/**
 * 添加banner弹出窗口
 */
@connect()
class AddModal extends Component {

  static defaultProps = {
    visible: false
  }

  handleOk = () => {
    this.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const formData = {
          ...values,
          start_time: values.time[0].format(),
          end_time:  values.time[1].format(),
        }
        delete formData.time
        this.props.dispatch({
          type: 'homebanner/addBanner',
          payload: formData
        }).then(() => {
          this.props.onChange(false)
        })
      }
    });
    // this.props.onChange(false)
  }

  handleCancel = () => {
    this.props.onChange(false)
  }

  saveForm = (form) => {
    if (form) {
      this.form = form.props.form
    }
  }

  render () {
    return (
      <Modal
        title="Basic Modal"
        visible={this.props.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <AddForm wrappedComponentRef={this.saveForm} />
      </Modal>
    )
  }
}

const mapStateToProps = (state) => state.homebanner;
// @routeConfig
@connect(mapStateToProps)
class HomeBanner extends Component {

  state = {
    name: '刘宇',
    visible: false
  }

  componentDidMount () {
    this.props.dispatch({
      type: 'homebanner/fetchBanner'
    })
  }

  render () {
    console.log(this)
    const {list} = this.props;

    return (
      <React.Fragment>
        <PageContent
          buttons={[
            <Button type='primary' icon='plus' onClick={() => this.setState({visible: true})}>添加活动</Button>
          ]}
          content={<BannerList list={list} />}
        />
        <AddModal visible={this.state.visible} onChange={(visible) => this.setState({visible: visible})} />
      </React.Fragment>
    )
  }

}

export default HomeBanner
