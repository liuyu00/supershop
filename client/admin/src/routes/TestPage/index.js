import React, {Component} from 'react';
import {Button} from 'antd';
import {connect} from 'dva';

const mapStaeToProps = (state) => state.testpage;

@connect(mapStaeToProps)
class TestPage extends Component {

  state = {
    name: '刘宇'
  }
  // constructor (props) {
  //   super(props)
  //   this.state = {

  //   }
  // }

  handlerClick = () => {
    this.props.dispatch({
      type: 'testpage/add_car_item',
      payload: {name: '商品'+this.props.carList.length}
    })
  }

  render () {
    console.log(this)
    const {carList} = this.props;

    return (
      <div>
        {this.state.name}
        <Button onClick={this.handlerClick} type="primary">添加</Button>
        <ul>
          {carList.map((item, index) => <li key={index}>{item.name}</li>)}
        </ul>
      </div>
    )
  }

}

export default TestPage
