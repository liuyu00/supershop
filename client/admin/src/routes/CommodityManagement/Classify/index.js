import React, {Component} from 'react';
import {Button, Card, Icon, Row, TreeSelect, Modal, Form, Input, Table, Divider, message} from 'antd';
import UploadIcon from '@/components/UploadIcon'
import {connect} from 'dva';

// import routeConfig from '@/components/RouteConfig'
import PageContent from '@/components/PageContent'

import styles from './style.less'

const mapStateToProps = (state) => state.classify;

@Form.create()
@connect(mapStateToProps)
class AddForm extends Component {

  treeData (data = []) {
    return data.map((item) => {
      const newData = {
        title: item.name,
        value: item.id,
        key: item.id
      }
      if (item.children) {
        newData.children = this.treeData(item.children)
      }
      return newData
    })
  }

  render () {
    const {getFieldDecorator} = this.props.form;
    return (
      <Form>
        <Form.Item label="分类名称">
          {getFieldDecorator('name', {})(<Input />)}
        </Form.Item>
        <Form.Item label="图标">
          {getFieldDecorator('icon', {})(<UploadIcon />)}
        </Form.Item>
        <Form.Item label="父级">
          {getFieldDecorator('parent_id', {
            initialValue: this.props.defaultValue
          })(<TreeSelect
              treeData={this.treeData(this.props.list)}
              treeNodeLabelProp="title"
          />)}
        </Form.Item>
      </Form>
    )
  }
}

@connect()
class AddModal extends Component {

  static defaultProps = {
    visible: false
  }

  handleOk = () => {
    this.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'classify/addClassify',
          payload: values
        }).then(() => {
          this.props.onChange(false)
          this.form.resetFields();
        })
      }
    });
    // this.props.onChange(false)
  }

  handleCancel = () => {
    this.props.onChange(false)
    this.form.resetFields();
  }

  saveForm = (form) => {
    if (form) {
      this.form = form.props.form
    }
  }

  render () {
    return (
      <Modal
        title="添加分类"
        visible={this.props.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <AddForm wrappedComponentRef={this.saveForm} defaultValue={this.props.defaultValue}/>
      </Modal>
    )
  }
}


// @routeConfig
@connect(mapStateToProps)
class Classify extends Component {

  state = {
    visible: false,
    defaultValue: ''
  }

  componentDidMount () {
    this.props.dispatch({
      type: 'classify/classifyList'
    }).then(() => {
      console.log(this.props.list);
    })
  }

  handleAddChildren = (value = '') => {
    this.setState({
      defaultValue: value
    }, () => {
      this.setState({
        visible: true
      })
    })
  }

  handleDeleteItem = (id) => {
    Modal.confirm({
      title: '确定要删除这个分类吗?',
      content: '想好了',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk: () => {
        this.props.dispatch({
          type: 'classify/deleteClassify',
          payload: {
            id: id
          }
        }).then(() => {
          message.success('删除成功');
        })
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  get render_table () {
    const columns = [
      {
        title: '标题',
        dataIndex: 'name'
      },
      {
        title: '图标',
        dataIndex: 'icon',
        render: (value) => {
          return (
            <img src={`http://localhost:3000${value}`} style={{width: 102, height: 102}} />
          )
        }
      },
      {
        title: '操作 ',
        render: (text, item) => (
          <span>
            <a href="javascript:;">编辑</a>
            <Divider type="vertical" />
            <a href="javascript:;" onClick={() => this.handleAddChildren(item.id)}>添加子项</a>
            <Divider type="vertical" />
            <a href="javascript:;" onClick={() => this.handleDeleteItem(item.id)}>删除</a>
          </span>
        ),
      },
    ]
    const data = this.props.list
    return (
      <Table rowKey="id" columns={columns} dataSource={data} />
    )
  }

  render () {
    return (
      <React.Fragment>
        <PageContent
          buttons={[
            <Button type='primary' icon='plus' onClick={() => this.setState({visible: true})}>添加分类</Button>
          ]}
          content={this.render_table}
        />
        <AddModal visible={this.state.visible} defaultValue={this.state.defaultValue} onChange={(visible) => this.setState({visible: visible})} />
      </React.Fragment>
    )
  }

}

export default Classify
