import React, {Component} from 'react';
import {Button, Card, Icon, Cascader, TreeSelect, Modal, Form, Input, Table, Divider, message} from 'antd';
import {connect} from 'dva';

// import routeConfig from '@/components/RouteConfig'
import PageContent from '@/components/PageContent'
import BaseForm from '@/components/BaseForm'
import Attrs from '@/components/Attrs'
import BannerUpload from '@/components/BannerUpload'

import styles from './style.less'
const mapStateToProps = (state) => {
  return {
    ...state.dmall,
    loading: state.loading
  }
};

@connect(mapStateToProps)
class DeatilModal extends Component {

  static defaultProps = {
    visible: false
  }

  handleOk = () => {
    this.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        values.category_id = values.categoryId[values.categoryId.length - 1]
        this.props.dispatch({
          type:'dmall/addProduct',
          payload: values
        }).then(() => {
          message.success('添加成功')
          this.props.onChange(false)
          this.form.resetFields();
        })
      }
    });
    // this.props.onChange(false)
    // this.props.onChange(false)
  }

  handleCancel = () => {
    this.props.onChange(false)
    this.form.resetFields();
  }

  saveForm = (form) => {
    if (form) {
      this.form = form.props.form
    }
  }

  treeData = (data) => {
    
    return data.map((item) => {
      const newData = {
        label: item.name,
        value: item.id,
        key: item.id
      }
      if (item.children) {
        newData.children = this.treeData(item.children)
      }
      return newData
    })
  }

  render () {
    const {wareDetail, classifyList} = this.props;
    return (
      <Modal
        title="商品详情"
        visible={this.props.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
      >
        <div>
        <BaseForm
          layout="horizontal"
          wrappedComponentRef={this.saveForm}
          items={[
            {
              label: '商品名称',
              field: 'name',
              options: {
                initialValue: wareDetail.name,
                rules: [{ required: true, message: '请输入商品名称' }],
              },
              content: <Input />
            },
            {
              label: '商品价格',
              field: 'price',
              options: {
                initialValue: wareDetail.price,
                rules: [{ required: true, message: '请输入商品价格' }],
              },
              content: <Input />
            },
            {
              label: '商品规格',
              field: 'specs',
              options: {
                initialValue: wareDetail.specs,
                rules: [{ required: true, message: '请添加商品规格' }],
              },
              content: <Attrs />
            },
            {
              label: '商品库存',
              field: 'stock',
              options: {
                rules: [{ required: true, message: '请输入商品库存' }],
              },
              content: <Input />
            },
            {
              label: '商品分类',
              field: 'categoryId',
              options: {
                rules: [{ required: true, message: '请求选择商品分类' }],
              },
              content:  <Cascader options={this.treeData(classifyList)}  />
            },
            {
              label: '图片展示',
              field: 'pictures',
              options: {
                initialValue: wareDetail.pictures
              },
              content: <BannerUpload />
            },
            {
              label: '商品详情',
              field: 'detail',
              options: {
                initialValue: wareDetail.detail
              },
              content: <div className={styles.detail} dangerouslySetInnerHTML={{__html: wareDetail.detail}} />
            }
          ]}
        />
        </div>
      </Modal>
    )
  }
}

// @connect(mapStateToProps)
// class DeatilModal extends Component {
//   static defaultProps = {
//     visible: false
//   }
//   // 点击确定
//   handleOk = () => {
//     this.form.validateFieldsAndScroll((err, values) => {
//       if (!err) {
//         console.log(values)
//         this.props.onClose(false)
//         // values.category_id = values.categoryId[values.categoryId.length - 1]
//       }
//     });
//     // this.props.onChange(false)
//   }

//   // 点击取消
//   handleCancel = () => {
//     this.props.onClose(false)
//   }

//   saveForm = (form) => {
//     if (form) {
//       this.form = form.props.form
//     }
//   }

//   render () {
//     const detail = this.props.wareDetail
//     return (
//       <Modal
//         title="商品详情"
//         visible={this.props.visible}
//         onOk={this.handleOk}
//         onCancel={this.handleCancel}
//       >
//         <BaseForm
//           layout="horizontal"
//           wrappedComponentRef={this.saveForm}
//           items={[
//             {
//               label: '商品名称',
//               field: 'name',
//               options: {
//                 initialValue: detail.name,
//                 rules: [{ required: true, message: '请输入商品名称' }],
//               },
//               content: <Input />
//             },
//             {
//               label: '商品价格',
//               field: 'price',
//               options: {
//                 initialValue: detail.price,
//                 rules: [{ required: true, message: '请输入商品价格' }],
//               },
//               content: <Input />
//             },
//             {
//               label: '商品规格',
//               field: 'specs',
//               content: <Attrs />
//             }
//           ]}
//         />
//       </Modal>
//     )
//   }
// }


// @routeConfig
@connect(mapStateToProps)
class Dmall extends Component {
  state = {
    visible: false
  }

  componentDidMount () {
    this.initData()
  }

  // 获取初始化数据
  async initData () {
    await this.props.dispatch({
      type: 'dmall/getClassify'
    })
    const data = await this.props.dispatch({
      type: 'dmall/getCategory'
    })
    await this.props.dispatch({
      type: 'dmall/getWare',
      payload: {
        page: 1,
        pageSize: 5,
        categoryId: data[0].categoryId
      }
    })

    console.log(this.props.ware)
  }

  handleTableChange = (pagination) => {
    this.props.dispatch({
      type: 'dmall/getWare',
      payload: {
        page: pagination.current,
        pageSize: 5,
        categoryId: this.props.categoryId
      }
    })
  }

  handleChange = (values) => {
    this.props.dispatch({
      type: 'dmall/getWare',
      payload: {
        page: 1,
        pageSize: 5,
        ...values
      }
    })
  }


  treeData = (data) => {
    return data.map((item) => {
      const newData = {
        title: item.categoryName,
        value: item.categoryId,
        key: item.categoryId
      }
      if (item.childCategoryList) {
        newData.children = this.treeData(item.childCategoryList)
      }
      return newData
    })
  }

  showDetail = (id) => {
    this.props.dispatch({
      type: 'dmall/getWareDetail',
      payload: {
        id
      }
    }).then(() => {
      this.setState({visible: true})
    })
  }

  get render_table () {
    const columns = [
      {
        title: '商品名称',
        dataIndex: 'wareName',
        render: (value, item) => {
          return (
            <dl className={styles.ware}>
              <dt><img src={`${item.wareImg}`} /></dt>
              <dd>
                <h2>{item.wareName}</h2>
                <span></span>
              </dd>
            </dl>
          )
        }
      },
      {
        title: '价格',
        dataIndex: 'warePrice'
      },
      {
        title: '销量',
        dataIndex: 'monthSales'
      },
      {
        title: '操作',
        dataIndex: 'action',
        render: (value, item) => (
          <span>
            <a href="javascript:;" onClick={() => this.showDetail(item.sku)}>详情</a>
          </span>
        )
      }
    ]
    const data = this.props.ware
    const loading = this.props.loading.global
    return (
      <Table
        rowKey="wareId"
        columns={columns}
        loading={loading}
        dataSource={data}
        pagination={this.props.pageInfo}
        onChange={this.handleTableChange}
      />
    )
  }


  get render_filter () {
    return (
      <BaseForm
        layout="inline"
        items={[
          {
            label: '名称',
            field: 'name',
            content: <Input />
          },
          {
            label: '类别',
            field: 'categoryId',
            content: <TreeSelect
              treeData={this.treeData(this.props.category)}
              treeNodeLabelProp="title"
              style={{ width: 300 }}
            />
          }
        ]}
        onValuesChange={this.handleChange}
      />
    )
  }

  render () {
    return (
      <React.Fragment>
        <PageContent
          filters={this.render_filter}
          content={this.render_table}
        />
        <DeatilModal visible={this.state.visible} onChange={(visible) => this.setState({visible:visible})} />
      </React.Fragment>
    )
  }

}

export default Dmall
