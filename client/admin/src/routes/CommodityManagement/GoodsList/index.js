import React, {Component} from 'react';
import {Button, Card, Icon, Row, TreeSelect, Modal, Form, Input, Table, Divider, message} from 'antd';
import {connect} from 'dva';

import PageContent from '@/components/PageContent'
import styles from './style.less'
import BaseForm from '../../../components/BaseForm';
const mapStateToProps = (state) => state.goods;

// @routeConfig
@connect(mapStateToProps)
class Goods extends Component {
  state = {
    visible: false
  }

  componentDidMount () {
    this.initData()
  }

  // 获取初始化数据
  async initData () {
    await this.props.dispatch({type: 'goods/getClassify'})
    await this.props.dispatch({
      type: 'goods/getProduct',
      payload: {
        page: 1,
        pageSize: 5
      }
    })

    console.log('初始化请求')
    
  }

  handleTableChange = (pagination) => {
    this.props.dispatch({
      type: 'goods/getProduct',
      payload: {
        page: pagination.current,
        pageSize: 5
      }
    })
  }

  handleFilter = (values) => {
    this.props.dispatch({
      type: 'goods/getProduct',
      payload: {
        page: 0,
        pageSize: 5,
        ...values
      }
    })
  }

  treeData = (data) => {
    return data.map((item) => {
      const newData = {
        label: item.name,
        value: item.id,
        key: item.id
      }
      if (item.children) {
        newData.children = this.treeData(item.children)
      }
      return newData
    })
  }

  get render_table () {
    const columns = [
      {
        title: '商品名称',
        dataIndex: 'wareName',
        render: (value, item) => {
          return (
            <dl className={styles.ware}>
              <dt><img src={`${item.cover}`} /></dt>
              <dd>
                <h2>{item.name}</h2>
                <span></span>
              </dd>
            </dl>
          )
        }
      },
      {
        title: '价格',
        dataIndex: 'warePrice'
      },
      {
        title: '销量',
        dataIndex: 'monthSales'
      }
    ]
    const products = this.props.products
    console.log(products)
    return (
      <Table
        rowKey="id"
        columns={columns}
        dataSource={products.data}
        pagination={products.pageInfo}
        onChange={this.handleTableChange}
      />
    )
  }
  get render_filters () {
    console.log(this.props.classifyList, '分类')
    return (
      <React.Fragment>
        <BaseForm
          onSubmit={this.handleFilter}
          items={
            [
              {
                label: '商品分离',
                field: 'categoryId',
                content: <TreeSelect
                  treeData={this.treeData(this.props.classifyList)}
                  treeNodeLabelProp="title"
                  style={{ width: 300 }}
                />
              }
            ]
          }
          buttons={[
            <Button htmlType="submit">搜索</Button>
          ]}
        />
      </React.Fragment>
    )
  }
  render () {
    return (
      <React.Fragment>
        <PageContent
          buttons={[
            <Button type='primary' icon='plus'>添加商品</Button>
          ]}
          filters={this.render_filters}
          content={this.render_table}
        />
      </React.Fragment>
    )
  }

}

export default Goods
