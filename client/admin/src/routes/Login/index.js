import React, {Component} from 'react';
import {Button, Card, Input, Form, DatePicker} from 'antd';
import {connect} from 'dva';
import BaseForm from '@/components/BaseForm'

import styles from './style.less'

const mapStaeToProps = (state) => state.global;

@Form.create()
class LoginForm extends Component{

  handlerLogin = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values)
      }
    });
    
  }

  render  () {
    const {getFieldDecorator} = this.props.form;
    
    return (
      <Form onSubmit={this.handlerLogin}>
        <Form.Item label="手机号">
          {getFieldDecorator('phone', {})(<Input type="text" />)}
        </Form.Item>
        <Form.Item label="密码">
          {getFieldDecorator('password', {})(<Input type="password" />)}
        </Form.Item>
        <Form.Item>
          <Button size="large" block type="primary" htmlType="submit" className="login-form-button">
            登录
          </Button>
        </Form.Item>
      </Form>
    )
  }
}

@connect(mapStaeToProps)
class Login extends Component {

  submit = (values) => {
    this.props.dispatch({
      type: 'global/login',
      payload: values
    }).then(() => {
      this.props.history.push({
        pathname: '/test'
      })
    })
  }

  render () {
    console.log(this.props)
    return (
      <div className={styles.login_page}>
        <Card style={{ width: 800 }}>
          <div className={styles.content_card}>
            <div>
              <img src="http://wechatapppro-1252524126.file.myqcloud.com/appAKLWLitn7978/image/cmVzb3VyY2UtY291cnNlQXJ0aWNsZS04MTE4MDA2MA.png" />
            </div>
            <div>
              <h2>登录</h2>
              <LoginForm onSubmit={this.submit} />
            </div>
          </div>
        </Card>
      </div>
    )
  }

}

export default Login
