import React, {Component} from 'react';

class UserLayout extends Component {
  render () {
    const children = this.props.children
    return (
      <div>
        UserLayout
        <div className="content">
          {children}
        </div>
      </div>
    )
  }

}

export default UserLayout
