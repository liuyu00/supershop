import React, {Component} from 'react';
import { Layout} from 'antd';

import styles from '../style.less'
const { Header} = Layout;

class HeaderBar extends Component {
  render () {

    console.log(this.props)

    return (
      <Header style={{ background: '#fff' }}>
        <img src="https://admin.xiaoe-tech.com/images/admin/helpCenter/xeLogo.png"  className={styles.logo} />
      </Header>
    )
  }

}

export default HeaderBar
