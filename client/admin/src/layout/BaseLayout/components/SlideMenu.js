import React, {Component} from 'react';
import { Layout, Menu, Icon } from 'antd';
import { withRouter } from 'dva/router';
import {menuConfig} from '@/commont/router.config'
import styles from '../style.less'

const { SubMenu } = Menu;
const { Sider } = Layout;

@withRouter
class SlideMenu extends Component {

  /**
   * 菜单选中事件
   */
  handlerMenuSelect = ({key}) => {
    const {history} = this.props
    history.push({
      pathname: key
    })
  }

  /**
   * 根据路由信息动态递归渲染menu菜单项
   * @param {Array} menuData 路由配置
   */
  renderMenu (menuData) {

    const menuItem = menuData.map( (item, index) => {
      if (item.children) {
        return (
          <SubMenu
            key={item.path}
            title={<span><Icon type="laptop" />{item.title}</span>}
          >
            {this.renderMenu(item.children)}
          </SubMenu>
        )
      }
      return <Menu.Item key={item.path}>{item.title}</Menu.Item>
    })

    return menuItem
  }
  
  render () {
    return (
      <Sider width={175} className={styles.slide}>
        <Menu
          mode="inline"
          onClick={this.handlerMenuSelect}
        >
          {this.renderMenu(menuConfig)}
        </Menu>
      </Sider>
    )
  }

}

export default SlideMenu
