import React, {Component} from 'react';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';

import {Redirect} from 'dva/router';

import HeaderBar from './components/HeaderBar'
import SlideMenu from './components/SlideMenu'

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

class BaseLayout extends Component {

  get isLogin () {
    return document.cookie.indexOf('token') !== -1
  }

  render () {
    const {children, location} = this.props

    console.log(this.props)

    return (
      <Layout>
        <HeaderBar />
        <Layout>
          <SlideMenu />
          <Layout style={{background: '#fff', marginLeft:175}}>
            <Breadcrumb style={{ margin: '16px 24px' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <Content
              style={{
                background: '#fff',
                margin: 0,
                minHeight: 280,
              }}
            >
              <div className="content">
                {this.isLogin ? children : <Redirect to={`/login?callback=${location.pathname}`} />}
              </div>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    )
  }

}

export default BaseLayout
