import React from 'react';
import {app} from './index'
import { Router } from 'dva/router';
import RouterView from '@/components/RouterView';
import routes from '@/commont/router.config.js'

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <RouterView routes={routes} />
    </Router>
  );
}

export default RouterConfig;
