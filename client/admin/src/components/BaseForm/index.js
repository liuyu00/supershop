/**
 * @description: 基础表单
 * @author: xianglin.tang@hand-china.com
 * @date: 2018-03-05 19:13:25
 */

import React from 'react';
import classNames from 'classnames';
import { Row, Col, Form, Icon } from 'antd';

import styles from './style.less';

const FormItem = Form.Item;
const BASE_FORMITEM_LAYOUT = {
    labelCol: { span: 11 },
    wrapperCol: { span: 13 },
};

/**
 * 基础表单
 * @prop {Array} items - 查询条件
 * @prop {Array} extraItems - 折叠查询条件
 * @prop {Array} buttons - 操作按钮
 * @prop {Object} formItemLayout - 布局参数
 * @prop {function} onSubmit - 表单提交时回调
 * @method getForm - 获取form对象
 */
@Form.create({
    onValuesChange (props, values, allValues) {
        props.onValuesChange && props.onValuesChange(allValues)
    }
})
export default class BaseForm extends React.PureComponent {
    static defaultProps = {
      layout: 'inline',
      onValuesChange: () => {}
    }
    constructor(props) {
        super(props);

        this.state = {
            fold: true,
        };
    }
    /**
     * 获取form对象
     */
    getForm = () => this.props.form;
    /**
     * 表单提交
     */
    handleFormSubmit = (e) => {
        e.preventDefault();

        const { form, onSubmit } = this.props;
        if (typeof onSubmit === 'function') {
            onSubmit(form.getFieldsValue(), form);
        }
    }
    /**
     * 展开/收起多余查询条件
     */
    handleFoldClick = () => {
        const { fold } = this.state;
        this.setState({ fold: !fold });
    }
    renderFormItems = (item) => {
        const { formItemLayout, form } = this.props;
        const { getFieldDecorator } = form;
        return (
            <FormItem
                label={item.label}
            >
                {getFieldDecorator(item.field, item.options)(
                    React.cloneElement(
                        item.content,
                        {
                            // style: { width: '100%' },
                        },
                    )
                )}
            </FormItem>
        );
    }
    renderItems = (items) => {
        if (!(items && items.length > 0)) {
            return null;
        }

        return items.map((one, i) => {
            const { lg = 6, md = 8, sm = 24 } = one.rowLayout || {};
            if (this.props.layout === 'vertical' || this.props.layout === 'horizontal') {
                return (
                    <React.Fragment key={one.field || i}>
                        {one.isFormItem === false ? one.content : (
                            this.renderFormItems(one)
                        )}
                    </React.Fragment>
                )
            }
            return (
                <Col
                    key={one.field || i}
                    lg={lg}
                    md={md}
                    sm={sm}
                    style={{ marginBottom: 10, ...one.style }}
                >
                    {one.isFormItem === false ? one.content : (
                        this.renderFormItems(one)
                    )}
                </Col>
            );
        });
    }
    render() {
        const { fold } = this.state;
        const { items, extraItems, buttons, layout } = this.props;
        const formItemLayout = layout === 'horizontal'
                ? {
                    labelCol: { span: 4 },
                    wrapperCol: { span: 14 },
                }
                : null;
        return (
            <Form
                layout={layout}
                className={styles.form}
                onSubmit={this.handleFormSubmit}
                {...formItemLayout}
            >
                <div
                    className={classNames(
                        styles.container, fold ? styles.fold : styles.unfold
                    )}
                >
                    <Row>
                        {this.renderItems(items)}
                    </Row>
                    {extraItems && (
                        <React.Fragment>
                            <Row className={styles.extraContainer}>
                                {this.renderItems(extraItems)}
                            </Row>
                            <Row className={styles.foldContainer}>
                                <p onClick={this.handleFoldClick}>
                                    {/* {fold ? formatMessage('baseForm.expand') : formatMessage('baseForm.unexpand')} */}
                                    <Icon
                                        type="down"
                                        style={{ marginLeft: 4 }}
                                    />
                                </p>
                            </Row>
                        </React.Fragment>
                    )}
                </div>
                {buttons && (
                    <div className={styles.buttons}>
                        {buttons.map((one, index) => {
                            return React.cloneElement(
                                one
                            );
                        })}
                    </div>
                )}
            </Form>
        );
    }
}
