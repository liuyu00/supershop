// import React, {Component} from 'react'
// import {Input, Icon} from 'antd';

// import styles from './style.less'

// const InputGroup = Input.Group;

// class Attrs extends Component {
//   state = {
//     attrs: [],
//     defaultValue: []
//   }

//   static getDerivedStateFromProps (props, state) {
//     if (JSON.stringify(props.value) === JSON.stringify(state.defaultValue)) {
//       return null
//     } else {
//       return {
//         attrs: props.value,
//         defaultValue: props.value,
//       }
//     }
//   }

//   handlerChangeAttrs = (index, type, value) => {
//     const attrs = this.state.attrs
//     attrs[index][type] = value
//     this.setState({
//       attrs: attrs
//     }, () => {
//       this.props.onChange(this.state.attrs)
//     })
//     // setTimeout(() => {
//     //   this.setState({
//     //     attrs: attrs
//     //   })
//     //   this.props.onChange(this.state.attrs)
//     // })
    
//   }

//   handlerAddAttrs = () => {
//     const attrs = this.state.attrs;
//     attrs.push({
//       key: '',
//       value: ''
//     })
//     this.setState({
//       attrs
//     }, () => {
//       this.props.onChange(this.state.attrs)
//     })
//   }

//   handlerReduceAttrs = (index) => {
//     const attrs = this.state.attrs;
//     attrs.splice(index, 1)
//     this.setState({
//       attrs
//     }, () => {
//       this.props.onChange(this.state.attrs)
//     })
//   }

//   render () {
//     return (
//       <div>
//         {this.state.attrs.map((item, index) => {
//           return (
//             <InputGroup compact className={styles.attrs} key={index}>
//               <Input value={item.key} onChange={(e) => this.handlerChangeAttrs(index, 'key', e.target.value)} style={{ width: 100 }} />
//               <span className={styles.line}>-</span>
//               <Input value={item.value} onChange={(e) => this.handlerChangeAttrs(index, 'value', e.target.value)} style={{ width: 100 }} />
//               <button onClick={(e) => this.handlerReduceAttrs(index)}><Icon type="minus" /></button>
//             </InputGroup>
//           )
//         })}
//         <button onClick={this.handlerAddAttrs}>添加规格</button>
//       </div>
//     )
//   }
// }

// export default Attrs


import React, {Component} from 'react'
import {Input, Icon} from 'antd';

import styles from './style.less'

const InputGroup = Input.Group;

class Arrts extends Component {
  state = {
    attrs: []
  }

  handlerAddAttrs = () => {
    const attrs = this.state.attrs;
    attrs.push({
      key: '',
      value: ''
    })
    this.setState({
      attrs
    }, () => {
      this.props.onChange(this.state.attrs)
    })
  }

  handlerReduceAttrs = (index) => {
    const attrs = this.state.attrs;
    attrs.splice(index, 1)
    this.setState({
      attrs
    }, () => {
      this.props.onChange(this.state.attrs)
    })
  }

  handlerChangeAttrs = (index, type, value) => {
    const attrs = this.state.attrs
    attrs[index][type] = value
    this.setState({
      attrs: attrs
    }, () => {
      this.props.onChange(this.state.attrs)
    })
  }

  render () {
    return (
      <div>
        {this.state.attrs.map((item, index) => {
          return (
            <InputGroup compact className={styles.attrs} key={index}>
              <Input value={item.key} style={{ width: 100 }} onChange={(e) => this.handlerChangeAttrs(index, 'key', e.target.value)} />
              <span className={styles.line}>-</span>
              <Input value={item.value} style={{ width: 100 }} onChange={(e) => this.handlerChangeAttrs(index, 'value', e.target.value)} />
              <button onClick={(e) => this.handlerReduceAttrs(index)}><Icon type="minus" /></button>
            </InputGroup>
          )
        })}
        <button onClick={this.handlerAddAttrs}>添加规格</button>
      </div>
    )
  }
}

export default Arrts
