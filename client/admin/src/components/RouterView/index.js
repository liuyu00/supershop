import React, {Component} from 'react';
import proptypes from 'prop-types'
import { Route, Switch } from 'dva/router';

class RouterView extends Component {

  static propTypes = {
    routes: proptypes.array
  }

  static defaultProps = {
    routes: []
  }

  render () {
    const {routes} = this.props;

    return (
      <React.Fragment>
        <Switch>
          {
            routes.map((item, index) => {
              return <Route path={item.path} render={(props) => {
                if (item.children) {
                  return (
                    <item.component {...props}>
                      <RouterView routes={item.children} />
                    </item.component>
                  )
                } else {
                  return <item.component {...props}/>
                }
              }} key={index} />
            })
          }
        </Switch>
      </React.Fragment>
    )
  }
}

export default RouterView;
