import React, {Component} from 'react';
import {Button, Card, Icon, Row, Col} from 'antd';
import style from './style.less'
import routeConfig from '@/components/RouteConfig'


@routeConfig
class PageContent extends Component {

  static defaultProps = {
    buttons: [],
    filters: '',
    content: ''
  }

  render () {
    return (
      <div>
        <header className={style.header}>
          <h2>{this.props.route.title}</h2>
          <div className={style.btn_group}>
            {this.props.buttons.map((item, index) => {
              return React.cloneElement(item, {
                key: index
              })
            })}
          </div>
        </header>
        <div className={style.content}>
          <div className={style.filter}>
            {this.props.filters}
          </div>
          <div>{this.props.content}</div>
        </div>
      </div>
    )
  }

}

export default PageContent