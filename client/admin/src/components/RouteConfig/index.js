import React, {Component} from 'react'
import {withRouter} from 'dva/router'
import routes from '@/commont/router.config'

const getRoutesDictionary = (routes, dictionary = {}) => {
  const routesDictionary = dictionary

  routes.forEach(item => {
    routesDictionary[item.path] = item
    if (item.children) {
      getRoutesDictionary(item.children, routesDictionary)
    }
  });

  return routesDictionary
}

const routeConfig = (OldComponent) => {
  @withRouter
  class NewComponent extends Component {

    state = {
      route: {}
    }

    componentDidMount () {
      const routesInfo = getRoutesDictionary(routes)
      this.setState({
        route: routesInfo[this.props.location.pathname]
      })
    }

    render () {
      return <OldComponent {...this.props} route={this.state.route} />
    }
  }

  return NewComponent
}

export default routeConfig
