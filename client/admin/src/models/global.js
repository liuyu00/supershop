import {login} from '@/services/user'

export default {

  namespace: 'global',

  state: {
    userInfo: {}
  },

  effects: {
    *login({ payload }, { call, put }) {  // eslint-disable-line
      console.log(payload)
      try {
        const data = yield call(login, payload)
        console.log(data)
      } catch (e) {
        return Promise.reject(e)
      }
    },
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
  },
};
