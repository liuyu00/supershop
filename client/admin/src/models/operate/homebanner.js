import {getBanner, postBanner} from '@/services/operate'

export default {

  namespace: 'homebanner',

  state: {
    list: []
  },

  effects: {
    *fetchBanner (actions, {call, put}) {
      const {data}= yield call(getBanner, {})
      console.log(data, 'module')
      yield put({type: 'set_banner_data', data})
    },
    *addBanner ({payload}, {call, put}) {
      yield call(postBanner, payload)
      yield put({type: 'fetchBanner'})
    }
  },

  reducers: {
    set_banner_data (state, {data}) {
      return Object.assign({}, state, {
        list: data
      })
    }
  },

};
