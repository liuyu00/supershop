
const fetchData = (item) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(item)
    }, 2000)
  })
}
export default {
  namespace: 'testpage',
  state: {
    carList: [
      {
        name: '商品1'
      },
      {
        name: '商品2'
      },
      {
        name: '商品3'
      },
      {
        name: '商品4'
      }
    ]
  },
  reducers: {
    add (state, {item}) {
      console.log('aaa')
      return Object.assign({}, state, {
        carList: state.carList.concat(item)
      })
    }
  },
  effects: {
    *add_car_item (actions, {call, put, select, apply}) {
      // 获取state
      const state = yield select(rootState => rootState.testpage)
      console.log(state)
      // 执行一个promise函数
      // const data = yield call(fetchData, actions.payload)
      const data = yield apply('', fetchData, [actions.payload])

      // 用来发送一个reducer的执行命令
      yield put({
        type: 'add',
        item: data
      })
    }
  }
}
