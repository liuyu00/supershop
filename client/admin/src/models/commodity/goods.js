import {getProduct, getClassify} from '@/services/commodity'

export default {

  namespace: 'goods',

  state: {
    products: {},
    classifyList: []
  },

  effects: {
    *getClassify ({payload}, {call, put}) {
      const data = yield call(getClassify)
      yield put({
        type: 'set_classify',
        data
      })
      return Promise.resolve(data)
    },
    *getProduct ({payload}, {call, put}) {
      const data = yield call(getProduct, payload)
      yield put({
        type: 'set_product_list',
        data
      })
    }
  },

  reducers: {
    set_product_list (state, {data, categoryId}) {
      return Object.assign({}, state, {
        products: data
      })
    },
    set_classify (state, {data}) {
      return Object.assign({}, state, {
        classifyList: data.data
      })
    }
  },

};
