import {getCategory, getWare, getWareDetail, addProduct, getClassify} from '@/services/commodity'

export default {

  namespace: 'dmall',

  state: {
    categoryId: '',
    category: [],
    classifyList: [],
    ware: [],
    pageInfo: {},
    wareDetail: {}
  },

  effects: {
    *getClassify ({payload}, {call, put}) {
      const data = yield call(getClassify)
      yield put({
        type: 'set_classify',
        data
      })
      return Promise.resolve(data)
    },
    *getCategory ({payload}, {call, put}) {
      const data = yield call(getCategory)
      yield put({
        type: 'set_category',
        data
      })
      return Promise.resolve(data)
    },
    *getWare ({payload}, {call, put}) {
      const data = yield call(getWare, payload)
      yield put({
        type: 'set_ware_list',
        data,
        categoryId: payload.categoryId
      })
    },
    *getWareDetail ({payload}, {call, put}) {
      const data = yield call(getWareDetail, payload)
      yield put({
        type: 'set_detail',
        data
      })
    },
    *addProduct ({payload}, {call, put}) {
      try {
        yield call(addProduct, payload)
        return Promise.resolve()
      } catch (e) {
        return Promise.reject()
      }
    }
  },

  reducers: {
    set_category (state, {data}) {
      return Object.assign({}, state, {
        category: data,
        categoryId: data[0].categoryId
      })
    },
    set_ware_list (state, {data, categoryId}) {
      return Object.assign({}, state, {
        ware: data.wareList,
        pageInfo: data.pageInfo,
        categoryId: categoryId
      })
    },
    set_detail (state, {data}) {
      return Object.assign({}, state, {
        wareDetail: data
      })
    },
    set_classify (state, {data}) {
      return Object.assign({}, state, {
        classifyList: data.data
      })
    }
  },

};
