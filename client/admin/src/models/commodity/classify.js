import {addClassify, getClassify, deleteClassify} from '@/services/commodity'

export default {

  namespace: 'classify',

  state: {
    list: []
  },

  effects: {
    *classifyList ({payload}, {call, put}) {
      const {data} = yield call(getClassify)
      yield put({
        type: 'set_classify_list',
        data
      })
    },
    *addClassify ({payload}, {call, put}) {
      yield call(addClassify, payload)
      yield put({
        type: 'classifyList'
      })
    },
    *deleteClassify ({payload}, {call, put}) {
      console.log(payload)
      yield call(deleteClassify, payload)
      yield put({
        type: 'classifyList'
      })
    }
  },

  reducers: {
    set_classify_list (state, {data}) {
      return Object.assign({}, state, {
        list: data
      })
    }
  },

};
