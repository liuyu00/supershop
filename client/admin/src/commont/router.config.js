
import dynamic from 'dva/dynamic';
import {app} from '@/index.js';
import BaseLayout from '@/layout/BaseLayout'
import Userlayout from '@/layout/UserLayout'


const loadComponent = (component, modules = []) => {
  return dynamic({
    app,
    models: () => modules.map(name => {
      console.log(name)
      return import(`@/models/${name}`)
    }),
    component
  });
}

const MainContent = ({children}) => {
  return (
    <div>{children}</div>
  )
}

const routes = [
  {
    path: '/login',
    component: loadComponent(() => import('@/routes/Login'))
  },
  {
    path: '/',
    component: BaseLayout,
    children: [
      {
        path: '/test',
        title: '测试页面',
        component: loadComponent(() => import('@/routes/TestPage'), ['testpage'])
      },
      {
        path: '/usermmmmm',
        title: '测试页面2',
        component: loadComponent(() => import('@/routes/TestPage'), ['testpage'])
      },
      {
        path: '/commodity',
        component: MainContent,
        title: '商品管理',
        children: [
          {
            path: '/commodity/classify',
            component: loadComponent(() => import('@/routes/CommodityManagement/Classify'), ['commodity/classify']),
            title: '分类管理'
          },
          {
            path: '/commodity/goods_list',
            component: loadComponent(() => import('@/routes/CommodityManagement/GoodsList'), ['commodity/goods']),
            title: '商品列表'
          },
          {
            path: '/commodity/dmall',
            component: loadComponent(() => import('@/routes/CommodityManagement/Dmall'), ['commodity/dmall']),
            title: '爬虫'
          }
        ]
      },
      {
        path: '/order',
        component: MainContent,
        title: '订单管理',
        children: [
          {
            path: '/order/list',
            component: loadComponent(() => import('@/routes/CommodityManagement/Classify'), ['testpage']),
            title: '订单列表'
          },
          {
            path: '/order/list2',
            component: loadComponent(() => import('@/routes/CommodityManagement/GoodsList'), ['testpage']),
            title: '退货申请'
          }
        ]
      },
      {
        path: '/operate',
        component: MainContent,
        title: '运营管理',
        children: [
          {
            path: '/operate/home_banner',
            component: loadComponent(() => import('@/routes/Operate/HomeBanner'), ['operate/homebanner']),
            title: '首页banner'
          },
          {
            path: '/operate/list2',
            component: loadComponent(() => import('@/routes/CommodityManagement/GoodsList'), ['testpage']),
            title: '快捷导航'
          }
        ]
      }
    ]
  }
  // {
  //   path: '/',
  //   component: Userlayout,
  //   children: [
  //     {
  //       path: '/login',
  //       component: loadComponent(() => import('@/routes/Login'), ['login'])
  //     }
  //   ]
  // }
]

export const menuConfig = routes[routes.length - 1].children

export default routes;
