const path = require('path')

export default {
  "extraBabelPlugins": [
    ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": true }],
    [
      "@babel/plugin-proposal-decorators",
      {
        "legacy": true
      }
    ]
  ],
  proxy: {
    '/api': {
      target: 'http://localhost:3000/'
    }
  },
  "theme": require('./src/theme.js'),
  "alias": {
    "@": path.resolve('./src')
  }
}
