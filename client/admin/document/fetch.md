## Fetch
浏览器新增的数据请求apiFetch API 提供了一个 JavaScript接口，用于访问和操纵HTTP管道的部分，例如请求和响应。它还提供了一个全局 fetch()方法，该方法提供了一种简单，合理的方式来跨网络异步获取资源。

这种功能以前是使用  XMLHttpRequest实现的。Fetch提供了一个更好的替代方法，可以很容易地被其他技术使用，例如 Service Workers。Fetch还提供了单个逻辑位置来定义其他HTTP相关概念，例如CORS和HTTP的扩展。

## 为什么使用fetch

XMLHttpRequest 是一个设计粗糙的 API，不符合关注分离（Separation of Concerns）的原则，配置和调用方式非常混乱，而且基于事件的异步模型写起来也没有现代的 Promise，generator/yield，async/await 友好。

Fetch 的出现就是为了解决 XHR 的问题，拿例子说明：

使用 XHR 发送一个 json 请求一般是这样：
```
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url);
  xhr.responseType = 'json';

  xhr.onload = function() {
    console.log(xhr.response);
  };

  xhr.onerror = function() {
    console.log("Oops, error");
  };

  xhr.send();
```
使用 Fetch 后，顿时看起来好一点
```
  fetch(url).then(function(response) {
    return response.json();
  }).then(function(data) {
    console.log(data);
  }).catch(function(e) {
    console.log("Oops, error");
  });
```

总结一下，Fetch 优点主要有：

* 语法简洁，更加语义化
* 基于标准 Promise 实现，支持 async/await

## Fetch语法

```
  fetch(url/request[, options])
  /**
  request 是一个 Request 对象，后面详细讲

  options 是一个对象，主要key 如下：
    method: GET/POST等
    headers: 一个普通对象，或者一个 Headers 对象
    body: 传递给服务器的数据，可以是字符串/Buffer/Blob/FormData，如果方法是 GET/HEAD，则不能有此参数
    mode: cors / no-cors / same-origin， 是否跨域，默认是 no-cors
    credentials: omit / same-origin / include  控制cookie
    cache: default / no-store / reload / no-cache / force-cache / only-if-cached
    redirect: follow / error / manual
    referrer: no-referrer / client / 或者一个url
    referrerPolicy: no-referrer / no-referrer-when-downgrade / origin /  origin-when-cross-origin / unsafe-url
    integrity: 资源完整性验证
  **/

  fetch()
  .then(response => {
    if (response.ok) {
    // 成功处理
    }
  }).catch(error => {
    // 异常处理
  })
```