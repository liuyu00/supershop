
const product = require('../model/product');
const Qs = require('qs')
const utils = require('../../utils/utils');
const axios = require('axios')
const FormData = require('form-data');

const category = async (req, res, next) => {
  const data = await product.category()
  res.json({
    code: 1,
    data: utils.arrayToTree(data)
  })
}

const add_category = async (req, res, next) => {
  const {
    name,
    icon,
    parent_id = 0
  } = req.body;
  try {
    await product.insertCategory({
      name,
      icon,
      parent_id
    })

    res.json({
      code: 1,
      msg: '添加成功'
    })
  } catch (e) {
    res.status(422).json({
      code: 0,
      msg: '添加失败'
    })
  }
}

const delect_category = async (req, res, next) => {
  const {
    id
  } = req.body;
  try {
    await product.delectCategory(id)

    res.json({
      code: 1,
      msg: '删除成功'
    })
  } catch (e) {
    res.status(422).json({
      code: 0,
      msg: '删除失败'
    })
  }
}

// axios.defaults.headers.post['content-type'] = 'application/x-www-form-urlencoded';
// axios.defaults.headers.get['content-type'] = 'application/x-www-form-urlencoded';
// axios.defaults.transformRequest = [function (data) {
//     let ret = ''
//     for (let it in data) {
//       ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
//     }
//     return ret
// }]


const get_category = async (req, res) => {
  const formData = {
    param: '{"from":1,"stores":[{"businessCode":1,"description":"超市商品，2小时达","isDefault":true,"label":"https://img.dmallcdn.com/bizItem/201906/5429839d-1b67-4ef5-aad9-eeb4b1e23343","logo":"https://img.dmallcdn.com/bizItem/201806/68fa1124-c08e-4fae-ba5e-704d790e1c7e","name":"多点超市","showType":1,"erpStoreId":206,"url":"https://cmsapi.dmall.com/app/web/json/1/206","venderId":1},{"businessCode":2,"description":"优质甄选，好货低价","label":"https://img.dmallcdn.com/bizItem/201906/5c562a8f-6fc4-4fc2-baeb-85d8ca3a2859","logo":"https://img.dmallcdn.com/bizItem/201610/1bf901f7-a57f-4176-ad53-16d709a10322","name":"全球精选","showType":1,"erpStoreId":206,"url":"https://cmsapi.dmall.com/app/web/ysjson/1/206","venderId":1}]}',
    source: 2,
    token: '',
    pubParam: '{}',
    tempid: 'C87A91BF6DE00002E48611CB9E20D3F0',
    d_track_data: '{"session_id":"C87B8F3C52B00002FDD51FEA16A11BB0","uuid":"C87A91BF6DE00002E48611CB9E20D3F0","project":"微信商城","env":"mweb","tdc":"","tpc":""}'
  }

  const categoryRes = await axios.post('https://searchgw.dmall.com/mp/wareCategory/list', formData, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
  })

  const category = categoryRes.data.data.wareCategory[0].categoryList
  res.json(category)
}

const get_ware_sarch = async (req, res) => {
  const {
    page = 1,
    categoryId,
    pageSize = 20
  } = req.query
  const formData = {
    param: `{"venderId":1,"storeId":206,"businessCode":1,"from":1,"categoryType":1,"pageNum":${page},"pageSize":${pageSize},"categoryId":${categoryId},"categoryLevel":1}`,
    source: 2,
    token: '',
    pubParam: '{}',
    tempid: 'C87A91BF6DE00002E48611CB9E20D3F0',
    d_track_data: '{"session_id":"C87B8F3C52B00002FDD51FEA16A11BB0","uuid":"C87A91BF6DE00002E48611CB9E20D3F0","project":"微信商城","env":"mweb","tdc":"","tpc":""}'
  }

  const wareData = await axios.post('https://searchgw.dmall.com/mp/search/wareSearch', formData, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
  })

  const data = wareData.data.data
  res.json({
    pageInfo: data.pageInfo,
    wareList: data.wareList
  })
}

const get_ware_detail = async (req, res) => {
  const {
    id
  } = req.query
  console.log(id)
  const formData = {
    param: `{"storeId":"206","skuId":${id},"moduleCodes":"slider,warebase,spec,shipment,tips,store,recommend,description","longitude":116.298802,"latitude":40.040916}`,
    source: 2,
    token: '',
    pubParam: '{}',
    tempid: 'C87A91BF6DE00002E48611CB9E20D3F0',
    d_track_data: '{"session_id":"C87B8F3C52B00002FDD51FEA16A11BB0","uuid":"C87A91BF6DE00002E48611CB9E20D3F0","project":"微信商城","env":"mweb","tdc":"","tpc":""}'
  }

  const wareData = await axios.post('https://detail.dmall.com/waredetail/main', formData, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
  })
  const data = wareData.data.data.moduleList
  const detailData = {
    pictures: data[0].data.wareImgList,
    name: data[1].data.name,
    oldPrice: data[1].data.offPrice,
    price: data[1].data.promotionPrice,
    specs: data[7].data.attributes,
    detail: data[7].data.description
  }
  res.json(detailData)
}


const add_ware = async (req, res, next) => {
  try {
    await product.insertWares(req.body)

    res.json({
      code: 1,
      msg: '添加成功'
    })
  } catch (e) {
    res.status(422).json({
      code: 0,
      msg: '添加失败'
    })
  }
}

const get_category_ids = (id, data) => {
  let ids = []
  // console.log(data)
  data.forEach((item) => {
    if (item.children) {
      item.children.forEach((subItem) => {
        if (subItem.id === id || item.id === id) {
          ids = ids.concat( get_category_ids(subItem.id, item.children)  )
        } else {
          ids = ids.concat( get_category_ids(id, item.children)  )
        }
      })
    } else if (item.id === id){
      ids.push(item.id)
    }
  })
  return ids
}

const select_ware = async (req, res, next) => {
  try {
    const {
      page = 0,
      pageSize = 10,
      ...filter
    } = req.query

    if (filter.categoryId) {
      const data = await product.category()
      const category_id = get_category_ids(filter.categoryId, utils.arrayToTree(data))
      delete filter.categoryId
      filter.category_id = category_id
    }

    const data = await product.selectWares({
      page,
      pageSize,
      filter
    })

    res.json({
      code: 1,
      ...data
    })
  } catch (e) {
    res.status(422).json({
      code: 0,
      msg: '添加失败'
    })
  }
}

module.exports = {
  add_category,
  category,
  delect_category,
  get_category,
  get_ware_sarch,
  get_ware_detail,
  add_ware,
  select_ware
}