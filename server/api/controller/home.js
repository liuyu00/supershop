
const home = require('../model/home');

const banner = async (req, res, next) => {
  const data = await home.banner()
  res.json({
    code: 1,
    data
  })
}

const add_banner = async (req, res, next) => {
  const {
    image,
    url,
    active_name,
    start_time,
    end_time
  } = req.body;
  console.log(req.body)
  try {
    await home.insert({
      image,
      url,
      active_name,
      start_time,
      end_time
    })

    res.json({
      code: 1,
      msg: '添加成功'
    })
  } catch (e) {
    res.status(422).json({
      code: 0,
      msg: '添加失败'
    })
  }
}


const quick_menu = async (req, res, next) => {

}

module.exports = {
  banner,
  add_banner,
  quick_menu
}