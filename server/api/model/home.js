const node_uid = require('node-uid');
const utils = require('../../utils/utils');
const db = require('../../db')

const insert = ({
  image,
  url,
  active_name,
  start_time,
  end_time
}) => {
  console.log(image,
    url,
    active_name,
    start_time,
    end_time)
  return new Promise((resolve, reject) => {
    db.query('insert into banner (id, image, url, active_name, start_time, end_time) values (?, ?, ?, ?, ?, ?)', [
      node_uid(),
      image,
      url,
      active_name,
      start_time,
      end_time
    ], (error, res) => {
      if (!error) {
        resolve(res)
      } else {
        reject(error)
      }
    })
  })
}

const banner = () => {
  return new Promise((resolve, reject) => {
    db.query('select * from banner', [], (error, res) => {
      if (!error) {
        resolve(utils.sqlListToObject(res))
      } else {
        reject(error)
      }
    })
  })
}

module.exports = {
  insert,
  banner
}