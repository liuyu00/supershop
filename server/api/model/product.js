const node_uid = require('node-uid');
const utils = require('../../utils/utils');
const db = require('../../db')

const insertCategory = ({
  name,
  icon,
  parent_id
}) => {
  console.log(name,
    icon,
    parent_id)
  return new Promise((resolve, reject) => {
    db.query('insert into category (id, name, icon, parent_id) values (?, ?, ?, ?)', [
      node_uid(),
      name,
      icon,
      parent_id
    ], (error, res) => {
      if (!error) {
        resolve(res)
      } else {
        reject(error)
      }
    })
  })
}

const category = () => {
  return new Promise((resolve, reject) => {
    db.query('select * from category', [], (error, res) => {
      if (!error) {
        resolve(utils.sqlListToObject(res))
      } else {
        reject(error)
      }
    })
  })
}

const delectCategory = (id) => {
  console.log(id);
  return new Promise((resolve, reject) => {
    db.query('DELETE FROM category WHERE id=?', [
      id
    ], (error, res) => {
      if (!error) {
        resolve(res)
      } else {
        reject(error)
      }
    })
  })
}


const insertWares = ({
  name, pictures, detail, price, old_price, stock, specs, category_id, label
}) => {
  console.log(name, pictures, detail, price, old_price, stock, specs, category_id, label)
  return new Promise((resolve, reject) => {
    db.query('insert into wares (name, cover, pictures, detail, price, old_price, stock, specs, category_id, label) values (?, ?, ?, ?,?, ?, ?, ?,?, ?)', [
      name,
      pictures[0],
      JSON.stringify(pictures),
      detail,
      price,
      old_price,
      parseInt(stock),
      JSON.stringify(specs),
      category_id,
      label
    ], (error, res) => {
      console.log(error)
      if (!error) {
        resolve(res)
      } else {
        reject(error)
      }
    })
  })
}

const selectWares = ({
  page = 0,
  pageSize = 10,
  filter
}) => {
  return new Promise((resolve, reject) => {
    let sql1 = 'select Count(*) from wares'
    let where = ''
    if (Object.keys(filter).length >= 1) {
      where += ` where `
      Object.keys(filter).forEach((key, index) => {
        if (key === 'category_id') {
          where += `${key} in (`
          filter[key].forEach((item) => {
            where += `'${item}',`
          })
          where = where.substr(0, where.length - 1)
          where += ') '
        } else {
          where += `${key}=${filter[key]} `
        }
      })
    }
    db.query(sql1 + where, (error, res) => {
      const count = Object.values(res[0])[0]
      let sql = `
        select * from wares ${where} limit ${page * pageSize},${pageSize}
      `
      console.log(sql)
      db.query(sql, [], (error, res) => {
        if (!error) {
          resolve({
            pageInfo: {
              pageNum: page * 1,
              pageSize: pageSize * 1,
              total: count
            },
            data: utils.sqlListToObject(res)
          })
        } else {
          reject(error)
        }
      })
    })

    
  })
}

module.exports = {
  insertCategory,
  category,
  insertWares,
  delectCategory,
  selectWares
}