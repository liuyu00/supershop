var express = require('express');
var router = express.Router();
var multer  = require('multer')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log('目录')
    cb(null, './public/uploads')
  },
  filename: function (req, file, cb) {
    // console.log('文件名', file)
    const fileName = file.originalname.split('.')
    cb(null,'img-' + Date.now() + '.' +fileName[1])
  }
})
var upload = multer({ storage })

const baseController = require('./controller/base')
const homeController = require('./controller/home')
const userController = require('./controller/user')
const productController = require('./controller/product')


// 公共接口
router.post('/upload', upload.single('file'), baseController.upload)

router.get('/home/banner', homeController.banner)
router.post('/home/banner', homeController.add_banner)

router.post('/product/category', productController.add_category)
router.get('/product/category', productController.category)
router.delete('/product/category', productController.delect_category)
router.post('/product/add_ware', productController.add_ware)
router.get('/product/select_ware', productController.select_ware)

router.get('/product/duodian/category', productController.get_category)
router.get('/product/duodian/ware', productController.get_ware_sarch)
router.get('/product/duodian/wareDetail', productController.get_ware_detail)



router.post('/register', userController.register)
router.post('/login', userController.login)


// 

/**
 * mvc (model view c)
 * 
 */

module.exports = router;
