const jwt = require('jsonwebtoken')
const whiteList = [
  '/api/login',
  '/api/register'
]
module.exports = (req, res, next) => {
  if (whiteList.indexOf(req.originalUrl) != -1) {
    next()
    return;
  }
  
  try {
    // var decoded = jwt.decode(req.headers.token, 'liuyu123456');
    jwt.verify(req.cookies.token, 'liuyu123456', (err, decoded) => {
      if (!err) {
        req.info = decoded
        next()
      } else {
        res.status(401).json({
          name: 'Unauthorized',
          message: '用户未登录',
          code: 0
        })
      }
    });
    
  } catch (e) {
    res.status(401).json({
      name: 'Unauthorized',
      message: '用户未登录',
      code: 0
    })
  }
}