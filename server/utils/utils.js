const sqlListToObject = (array) => {
  return array.map((item) => {
    const obj = {}
    for (let key in item) {
      try {
        obj[key] = JSON.parse(item[key])
      } catch (e) {
        obj[key] = item[key]
      }
    }
    return obj
  })
}

const arrayToTree = (data) => {
  const newData = []
  const dataJson = {}

  data.forEach(item => {
    dataJson[item.id] = item
  });

  data.forEach(item => {
    const parentId = item.parent_id
    if (parentId != 0) {
      if (dataJson[parentId].children) {
        dataJson[parentId].children.push(item)
      } else {
        dataJson[parentId].children = [item]
      }
    } else {
      newData.push(item)
    }
  });
  return newData
}

module.exports = {
  sqlListToObject,
  arrayToTree
}
